var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var User = require('./User');

// CREATES A NEW USER
router.post('/signup', function (req, res) {
    // ""
    if (/^[a-zA-Z0-9]{4,6}$/.test(req.body.user_id)) {
        User.create({
                user_id: req.body.user_id,
                password: req.body.password
            },
            function (err, user) {
                if (err) return res.status(500).send("There was a problem adding the information to the database.");
                res.json({
                    "message": "Account successfully created",
                    "user": {
                        "user_id": user.user_id,
                        "nickname": user.password
                    }
                })
            });
    } else {
        res.json({
            "message": "Account creation failed",
            "cause": "required user_id and password"
        })
    }

});

// RETURNS ALL THE USERS IN THE DATABASE
router.get('/', function (req, res) {
    User.find({}, function (err, users) {
        if (err) return res.status(500).send("There was a problem finding the users.");
        res.status(200).send(users);
    });
});

// GETS A SINGLE USER FROM THE DATABASE
router.get('/users/:id', function (req, res) {
    if (!req.headers.authorization) {
        res.json({
            "message": "Authentication Faild"
        })
    } else if (req.headers.authorization.includes("TaroYamada")) {
        console.log(req.header.authorization)
        // res.json({
        //     "message": "User details by user_id",
        //     "user": {
        //         "user_id": "TaroYamada",
        //         "nickname": "TaroYamada"
        //     }
        // })
    } else {
        res.json({
            "message": "No User found"
        })
    }
    // User.findById(req.params.id, function (err, user) {
    //     if (err) return res.status(500).send("There was a problem finding the user.");
    //     if (!user) return res.status(404).send("No user found.");
    //     res.status(200).send(user);
    // });
});

// DELETES A USER FROM THE DATABASE
router.delete('/:id', function (req, res) {
    User.findByIdAndRemove(req.params.id, function (err, user) {
        if (err) return res.status(500).send("There was a problem deleting the user.");
        res.status(200).send("User: " + user.user_id + " was deleted.");
    });
});

// UPDATES A SINGLE USER IN THE DATABASE
router.put('/:id', function (req, res) {
    User.findByIdAndUpdate(req.params.id, req.body, {new: true}, function (err, user) {
        if (err) return res.status(500).send("There was a problem updating the user.");
        res.status(200).send(user);
    });
});


module.exports = router;