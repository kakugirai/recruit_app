var mongoose = require('mongoose');  
var UserSchema = new mongoose.Schema({  
  user_id: String,
  password: String,
  nickname: String,
  comment: String
});
mongoose.model('User', UserSchema);

module.exports = mongoose.model('User');